#!/usr/bin/env python
import roslib
import rospy
import threading
import time
import numpy as np
import numpy
import os
import sys
from std_msgs.msg import Float64;
import serial
import math
from numpy.core.defchararray import rstrip
from pygments.styles import arduino
from std_srvs.srv import Empty


# configure the serial connections (the parameters differs on the device you are connecting to)
arduino = serial.Serial(
    port='/dev/ttyACM0',
    baudrate=14400,
    parity=serial.PARITY_ODD,
    stopbits=serial.STOPBITS_TWO,
    bytesize=serial.SEVENBITS
)

arduino.isOpen()

class takktile_arduino:
  def __init__(self):
      
    self.data = 0.0
    self.input = True
    
    self.loop = 0
    self.calibration_value = 0
        
    topic = 'takktile'
    self.one_calibrated_pub = rospy.Publisher(topic + '/takktile_sensor_one_measurement',  Float64, queue_size=1)
    self.two_calibrated_pub = rospy.Publisher(topic + '/takktile_sensor_two_measurement',  Float64, queue_size=1)
    self.three_calibrated_pub = rospy.Publisher(topic + '/takktile_sensor_three_measurement',  Float64, queue_size=1)
    self.four_calibrated_pub = rospy.Publisher(topic + '/takktile_sensor_four_measurement',  Float64, queue_size=1)
    self.five_calibrated_pub = rospy.Publisher(topic + '/takktile_sensor_five_measurement',  Float64, queue_size=1)
    self.six_calibrated_pub = rospy.Publisher(topic + '/takktile_sensor_six_measurement',  Float64, queue_size=1)
    self.seven_calibrated_pub = rospy.Publisher(topic + '/takktile_sensor_seven_measurement',  Float64, queue_size=1)
    self.eight_calibrated_pub = rospy.Publisher(topic + '/takktile_sensor_eight_measurement',  Float64, queue_size=1)
    self.nine_calibrated_pub = rospy.Publisher(topic + '/takktile_sensor_nine_measurement',  Float64, queue_size=1)
    self.ten_calibrated_pub = rospy.Publisher(topic + '/takktile_sensor_ten_measurement',  Float64, queue_size=1)
    self.eleven_calibrated_pub = rospy.Publisher(topic + '/takktile_sensor_eleven_measurement',  Float64, queue_size=1)
  def execute(self):
           
    self.input=True
    while True :
         
        # get data and process 
        # read the sensor measurement
        self.data = arduino.readline()   
        
        if self.loop >30:
            # trim the bracket []
            self.data = ''.join(c for c in self.data if c not in '[]\r\n')
            # transfer the data separated by comma into float
            self.data = [float(x) for x in self.data.split(',') if x]
            
            # get the initial value for initial calibration
            if self.loop == 31:
                
                self.calibration_value = [ -x for x in self.data]
            
            # call the zero out service if necesssary 
            
            #rospy.Service('takktile/zero', Empty, self.zero_callback)

            self.data = [x + y for x, y in zip(self.data, self.calibration_value)]

# 	    print self.data
            
            # publish the 'floated' sensor reading data, the unit of which has been transtfered to Newton
            self.one_calibrated_pub.publish(-self.data[0]/100)
            self.two_calibrated_pub.publish(-self.data[1]/100)
            self.three_calibrated_pub.publish(-self.data[2]/100)
            self.four_calibrated_pub.publish(-self.data[3]/100)
            self.five_calibrated_pub.publish(-self.data[3]/100)
#             self.six_calibrated_pub.publish(-self.data[0]/100)
#             self.seven_calibrated_pub.publish(-self.data[4]/100)
#             self.eight_calibrated_pub.publish(-self.data[5]/100)
#             self.nine_calibrated_pub.publish(-self.data[6]/100)
#             self.ten_calibrated_pub.publish(-self.data[7]/100)
#             self.eleven_calibrated_pub.publish(-self.data[8]/100)

            self.six_calibrated_pub.publish(-self.data[4]/100)
            self.seven_calibrated_pub.publish(-self.data[5]/100)
            self.eight_calibrated_pub.publish(-self.data[6]/100)
            self.nine_calibrated_pub.publish(-self.data[7]/100)
            self.ten_calibrated_pub.publish(-self.data[8]/100)
            self.eleven_calibrated_pub.publish(-self.data[9]/100)




        self.loop+=1
        
    def zero_callback(self, msg):
        
        self.calibration_value = [ -x for x in self.data]
        return []

# create the service for zeroing out the deviation of sensor measurements
def takktile_arduino_zero():

    rospy.wait_for_service('/takktile/zero')
    try:
        zero = rospy.ServiceProxy('/takktile/zero', Empty)
        zero()
        return
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e

if __name__ == '__main__':
    rospy.init_node('takktile_arduino', anonymous=True)
    try:  
        node = takktile_arduino()
        node.execute()    
    except rospy.ROSInterruptException:
        print("Shutting down!!!")
        pass
        arduino.close()
   





