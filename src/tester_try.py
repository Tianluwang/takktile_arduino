#!/usr/bin/env python
import serial, time
from std_msgs.msg import Float64
import math
from numpy.core.defchararray import rstrip

# open the serial port of arduino
arduino = serial.Serial('/dev/ttyACM0', 115200)
print "Connection successful"

#read data from arduino
loop = 1
while True:
  data = arduino.readline() 
  
  # trim the bracket []
  data = ''.join(c for c in data if c not in '[]\r\n')
  
  # transfer the data separated by comma into float
  data = [float(x) for x in data.split(',') if x]
  
  # pubish the data only after certain loops, just in case that invalid measurement exits when the port just opens
  if loop > 30:
     print data
  
  loop+=1


     
