#!/usr/bin/env python
import serial, time
from std_msgs.msg import Float64
import math

# open the serial port of arduino
arduino = serial.Serial('/dev/ttyACM0', 115200)
print "Connection successful"

#read data from arduino
loop = 1
while True:
  data = arduino.readline()   
  
  # get the raw data first
  takktile_one   = data[3:6]
  takktile_two   = data[10:13]
  takktile_three = data[17:20]
  takktile_four  = data[24:27]
  takktile_five  = data[31:34]
  takktile_six   = data[38:41]
  
  # only keep the digital characters
  takktile_one   = ''.join(ch for ch in takktile_one if ch.isdigit())
  takktile_two   = ''.join(ch for ch in takktile_two if ch.isdigit())
  takktile_three = ''.join(ch for ch in takktile_three if ch.isdigit())
  takktile_four  = ''.join(ch for ch in takktile_four if ch.isdigit())
  takktile_five  = ''.join(ch for ch in takktile_five if ch.isdigit())
  takktile_six   = ''.join(ch for ch in takktile_six if ch.isdigit())

  # transder the sting data to float
  if loop>10:
    takktile_one = float(takktile_one)
    takktile_two = float(takktile_two)
    takktile_three = float(takktile_three)
    takktile_four = float(takktile_four)
    takktile_five = float(takktile_five)
    takktile_six = float(takktile_six)

  print takktile_one, takktile_two, takktile_three, takktile_four, takktile_five, takktile_six
    




     
