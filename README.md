# takktile_arduino

Author(s): Tianlu Wang

Maintainer(s): Tianlu Wang ([tiawang@ethz.ch](mailto:tiawang@ethz.ch)), Martin Wermelinger ([martiwer@ethz.ch](mailto:martiwer@ethz.ch))

This package contains the interface for reading measurement data from TakkTile sensors through Arduino. The node can be launched by 
```
#!bash
roslaunch takktie_arduino takktile_arduino.launch

```
